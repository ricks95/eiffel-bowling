note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	GAME_TEST_SET

inherit
	EQA_TEST_SET


feature -- Test routines

	test_gutter_game
			-- New test routine
		local
			test_game : GAME
			points : INTEGER
		do
			create test_game.make
			points := test_game.score
			assert ("punti = 0", points = 0)
		end

end


