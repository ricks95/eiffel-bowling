note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

create
	make

feature
	points_array : ARRAY[INTEGER]

	make
		do
		    create points_array.make (0, 19)
		    points_array.clear_all
		end

	score : INTEGER
		local
			res : INTEGER
			i : INTEGER
		do
			res := 0
			from
				i := 0
			until
				i > 19
			loop
				res := res + points_array.item (i)
				i := i + 1
			end
		end

	roll (pins : INTEGER)
		do
			
		end
end
